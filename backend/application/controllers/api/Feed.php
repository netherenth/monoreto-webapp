<?php

class Feed extends CI_Controller
{
    public function __construct()
    {
        header('Access-Control-Allow-Origin: *');
        header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
        header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept");
        parent::__construct();
    }

    public function index()
    {
        $this->load->model('FeedModel', 'feed');

        if (!is_null($this->input->get('random'))) {
            $data = $this->feed->getRandomPost();
        } else {
            $offset = $this->input->get('offset');
            if (is_null($offset)) {
                $offset = '0';
            }
            $data = $this->feed->getRandom($offset);
        }

        foreach ($data as $idx => $item) {
            $item->id = uniqid();
            $likes = $item->likes;
            $views = $likes * 12;
            $profileViews = $views * rand(2, 3);
            $views = substr($views, 0, 3) . 'K';
            $item->liked = !is_null($this->input->get('random')) ? false : !!rand(0, 1);
            $item->views = $views;
            $item->profile_views = substr($profileViews, 0, 3) . 'K';
            $item->invested = (int) ($likes / 18);
            $item->comments = $this->getRandomComments();
        }

        $liked = $this->input->get('liked');
        if ($liked === '1') {
            $data = array_filter($data, function ($x) {
                return $x->liked === true;
            });
            $data = array_values($data);
        }

        shuffle($data);
        
        return $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($data));
    }

    private function getRandomComments()
    {
        $src = [
            [
                'id' => 1,
                'userpic' => '40w.jpg',
                'username' => 'angelaglossow',
                'text' => 'Nice use of blue in this shot :-)'
            ],
            [
                'id' => 2,
                'userpic' => '41w.jpg',
                'username' => 'morganstar',
                'text' => "I think I'm crying. It's that radiant."
            ],
            [
                'id' => 3,
                'userpic' => '42w.jpg',
                'username' => 'amywhite',
                'text' => 'This shot has navigated right into my heart.'
            ],
            [
                'id' => 4,
                'userpic' => '45m.jpg',
                'username' => 'kevinash',
                'text' => 'I want to learn this kind of shade! Teach me.'
            ],
            [
                'id' => 5,
                'userpic' => '43w.jpg',
                'username' => 'ashleytor',
                'text' => 'My 51 year old cousin rates this experience very fabulous, friend.'
            ],
            [
                'id' => 6,
                'userpic' => '44w.jpg',
                'username' => 'miriangisley',
                'text' => 'Elegant. So sleek.'
            ],
            [
                'id' => 7,
                'userpic' => '48m.jpg',
                'username' => 'shanedogster',
                'text' => 'Fresh work you have here.'
            ],
            [
                'id' => 8,
                'userpic' => '44w.jpg',
                'username' => 'catlover',
                'text' => 'I admire your concept dude'
            ],
            [
                'id' => 9,
                'userpic' => '32m.jpg',
                'username' => 'gregorypostman',
                'text' => 'Such animation, many layers, so sublime'
            ],
            [
                'id' => 10,
                'userpic' => '30w.jpg',
                'username' => 'elizawoods',
                'text' => 'Let me take a nap... great style, anyway.'
            ],
            [
                'id' => 11,
                'userpic' => '33w.jpg',
                'username' => 'janemotley',
                'text' => 'Engaging, friend. I adore the use of typography and background image!'
            ],
            [
                'id' => 12,
                'userpic' => '36m.jpg',
                'username' => 'johnred',
                'text' => 'Vastly thought out! I think clients would love this.'
            ],
            [
                'id' => 13,
                'userpic' => '26m.jpg',
                'username' => 'mikehammilton',
                'text' => 'Engaging. It keeps your mind occupied while you wait.'
            ],
            [
                'id' => 14,
                'userpic' => '39w.jpg',
                'username' => 'katewilson',
                'text' => 'Gradient, pattern, shot, colours – beastly.'
            ],
        ];

        $comments = [];
        $commentIds = [];
        foreach (range(0, rand(0, 4)) as $_) {
            $comment = $src[rand(0, 13)];
            while (in_array($comment['id'], $commentIds)) {
                $comment = $src[rand(0, 13)];
            }
            $commentIds[] = $comment['id'];
            $comments[] = $comment;
        }
        return $comments;
    }
}
