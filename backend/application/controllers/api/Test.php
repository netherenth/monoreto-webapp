<?php

class Test extends CI_Controller
{
    public function index ()
    {
        $avatars = glob(__DIR__ . '/mono_avatars/*');
        $postImages = glob(__DIR__ . '/mono_feed/*');

        $names = [
            "kathiefreddie",
            "daphnereilly",
            "vinalkyler",
            "aliseabigail",
            "kassieunique",
            "natanielbrion",
            "daniella",
            "patriciawheel",
            "elinorerobin",
            "stewardhaylan",
            "marniesalome",
            "aletatricia",
            "jolenedella",
            "zacharian",
            "lannyscott",
            "katewilson",
            "kristaleola",
            "elizabethtiana",
            "traciejakie",
            "joiseelila",
            "marianell",
            "letaloretta",
            "marleensapphire",
            "marylinnluna",
            "lexiangie",
            "zenakylie",
            "daveogloe",
            "alissarandy",
            "patronquille",
            "edietoria",
            "abbeylaravy",
            "sharilacy",
            "suzanneboy",
            "galeyyazmin",
            "tannermaurice",
            "richrelya",
            "bryanotis",
            "jayharper",
            "lawrencerich",
            "normbishop",
            "ronisalina",
            "jackcherokee",
            "shelbeysyd",
            "osborneadair",
            "davemadison"];

        $result = [];
        foreach ($postImages as $file) {
            $r = array_rand($names);
            $avatar = str_replace('/home/svartr/Projects/js/mobile-app-presentation/backend/application/controllers/api/mono_avatars/', '', $avatars[$r]);
            $username = $names[$r];
            
            $result[] = [
                'avatar' => $avatar,
                'username' => $username,
                'image' => str_replace('/home/svartr/Projects/js/mobile-app-presentation/backend/application/controllers/api/mono_feed/', '', $file),
            ];
        }

        foreach ($result as $item) {
            $this->db->insert('feed', [
                'avatar' => $item['avatar'],
                'image' => $item['image'],
                'username' => $item['username'],
                'text' => 'Maecenas nec odio et ante tincidunt tempus. Donec vitae sapien ut libero venenatis faucibus. Nullam quis ante. Etiam sit amet orci eget eros faucibus tincidunt. Duis leo. Sed fringilla mauris sit amet nibh. Donec sodales sagittis magna. Sed consequat, leo eget bibendum sodales, augue velit cursus nunc',
                'likes' => rand(10000, 40000),
            ]);
        }
    }
}
