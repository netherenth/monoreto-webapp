<?php

class Charity extends CI_Controller
{
    public function __construct()
    {
        header('Access-Control-Allow-Origin: *');
        header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
        parent::__construct();
    }

    public function index()
    {
        $this->load->model('CharityModel', 'charity');

        $offset = $this->input->get('offset');
        if (is_null($offset)) {
            $offset = '0';
        }

        $data = $this->charity->getRandom($offset);
        foreach ($data as $idx => $item) {
            $item->id = uniqid();
            $likes = $item->likes;
            $views = $likes * 12;
            $profileViews = $views * rand(2, 3);
            $views = substr($views, 0, 3) . 'K';
            $item->liked = !!$idx;
            $item->views = $views;
            $item->comments_count = substr($likes, 1, 1);
            $item->profile_views = substr($profileViews, 0, 3) . 'K';
            $item->invested = (int) ($likes / 18);
        }

        $liked = $this->input->get('liked');
        if ($liked === '1') {
            $data = array_filter($data, function ($x) {
                return $x->liked === true;
            });
            $data = array_values($data);
        }

        return $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($data));
    }
}
