<?php

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

require __DIR__ . './../../../vendor/autoload.php';

class Register extends CI_Controller
{
    public function __construct()
    {
        header('Access-Control-Allow-Origin: *');
        header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
        parent::__construct();
    }


    public function index()
    {
        $email = $this->input->post('email');
        $this->load->model('AccountModel', 'account');

        $mail = new PHPMailer(true);
        try {
            $mail->isSMTP();                                      // Set mailer to use SMTP
            $mail->Host = 'smtp.yandex.ru';  // Specify main and backup SMTP servers
            $mail->SMTPAuth = true;                               // Enable SMTP authentication
            $mail->Username = '';                 // SMTP username
            $mail->Password = '';                           // SMTP password
            $mail->SMTPSecure = 'ssl';                            // Enable TLS encryption, `ssl` also accepted
            $mail->Port = 465;                                    // TCP port to connect to
        
            //Recipients
            $mail->setFrom('', 'Monoreto');
            $mail->addAddress($email);     // Add a recipient
            
            //Content
            $mail->isHTML(false);                                  // Set email format to HTML
            $mail->Subject = 'Monoreto Registration';
            $mail->Body    = 'Welcome to Monoreto. We will email you when new features are available. Stay tuned!';
            $mail->AltBody = 'Welcome to Monoreto. We will email you when new features are available. Stay tuned!';
        
            $mail->send();
        } catch (Exception $e) {}

        $this->account->register($email);

        return $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($data));
    }
}
