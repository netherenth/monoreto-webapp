<?php

class CharityModel extends CI_Model
{
    public $image;
    public $username;
    public $short_desc;
    public $description;
    public $likes;

    public function getRandom($offset)
    {
        $query = $this->db->get('charity', 10, $offset * 10);
        return $query->result();
    }
}
