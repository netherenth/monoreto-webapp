<?php

class AccountModel extends CI_Model
{
    public $id;
    public $email;
    public $created_at;

    public function register($email)
    {
        $this->db->insert('accounts', [
            'email' => $email,
            'created_at' => time(),
        ]);
    }
}
