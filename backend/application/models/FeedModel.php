<?php

class FeedModel extends CI_Model
{
    public $image;
    public $userpic;
    public $username;
    public $text;

    public function getRandom($offset)
    {
        $query = $this->db->get('feed', 20, $offset * 20);
        return $query->result();
    }

    public function getRandomPost()
    {
        $totalCount = $this->db->count_all_results('feed');
        $randOffset = rand(0, $totalCount - 1);

        $query = $this->db->get('feed', 1, $randOffset);
        return $query->result();
    }

    public function getTop($offset)
    {
        $query = $this->db->order_by('likes', 'DESC')->get('feed', 10, $offset * 10);
        return $query->result();
    }
}
