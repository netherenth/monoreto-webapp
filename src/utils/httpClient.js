import axios from 'axios'

export default axios.create({
  // baseURL: 'http://monoreto-backend.local/api/',
  baseURL: 'http://netherenth.xyz:8012/index.php/api/',
  timeout: 5000,
  headers: {}
})
