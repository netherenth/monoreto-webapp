
const isScrolledToBottom = () => {
  const scrollY = window.scrollY
  const visible = document.documentElement.clientHeight
  const pageHeight = document.documentElement.scrollHeight
  const bottomOfPage = visible + scrollY >= pageHeight - 300
  return bottomOfPage || pageHeight < visible
}

export { isScrolledToBottom }
