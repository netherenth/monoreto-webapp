export default class Swipe {
  constructor(element) {
    this.xUp = null
    this.yUp = null
      this.xDown = null;
      this.yDown = null;
      this.element = typeof(element) === 'string' ? document.querySelector(element) : element;

      this.element.addEventListener('touchstart', function(evt) {
          this.xDown = evt.touches[0].clientX;
          this.yDown = evt.touches[0].clientY;
      }.bind(this), false);

    this.element.addEventListener('mousedown', function(evt) {
        this.xDown = evt.clientX;
        this.yDown = evt.clientY;
    }.bind(this), false);

      this.handleTouchMove = this.handleTouchMove.bind(this);
      this._handle = this._handle.bind(this)
  }

  onLeft(callback) {
      this.onLeft = callback;

      return this;
  }

  onRight(callback) {
      this.onRight = callback;

      return this;
  }

  onUp(callback) {
      this.onUp = callback;

      return this;
  }

  onDown(callback) {
      this.onDown = callback;

      return this;
  }

  _handle (xDiff, yDiff) {

    if ( Math.abs( xDiff ) > Math.abs( yDiff ) ) { 
        if (xDiff > 0) {
            if (typeof this.onLeft !== 'undefined') {
                this.onLeft();
            }
        } else {
            if (typeof this.onRight !== 'undefined') {
                this.onRight();
              }
        }
    } else {
        if ( yDiff > 0 ) {
            if (typeof this.onUp !== 'undefined') {
              this.onUp();
            }
        } else {
          if (typeof this.onDown !== 'undefined') {
            this.onDown();
          }
        }
    }

    // Reset values.
    this.xDown = null;
    this.yDown = null;
  }

  handleTouchMove(evt) {
      if ( ! this.xDown || ! this.yDown ) {
          return;
      }

      this.xUp = evt.touches[0].clientX;
      this.yUp = evt.touches[0].clientY;

      this.xDiff = this.xDown - this.xUp;
      this.yDiff = this.yDown - this.yUp;

      this._handle(this.xDiff, this.yDiff)
  }

  handleMouseUp (e) {
    let xDiff = this.xDown - e.clientX;
    let yDiff = this.yDown - e.clientY;

    this._handle(xDiff, yDiff)
  }

  run() {
    this.element.addEventListener('touchmove', this.handleTouchMove.bind(this), false);

    this.element.addEventListener('mouseup', this.handleMouseUp.bind(this), false);
  }
}
