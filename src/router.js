import Vue from 'vue'
import VueRouter from 'vue-router'
import Feed from './components/Feed/Feed'
import TopContent from './components/TopContent/TopContent'
import Investment from './components/Investment/Investment'
import Charity from './components/Charity/Charity'
import Home from './components/Home/Home'
import AddFeedPost from './components/Feed/AddPost'
import Register from './components/shared/Register'
import Login from './components/shared/Login'

Vue.use(VueRouter)

const router = new VueRouter({
  routes: [
    // {
      // path: '/',
      // component: App,
      // children: [
        {
          path: '',
          component: Home,
          name: 'home',
          meta: { title: 'Profile' }
        },
        {
          path: '/feed',
          component: Feed,
          name: 'feed',
          meta: { title: 'Feed' }
        },
        {
          path: '/top',
          component: TopContent,
          name: 'top',
          meta: { title: 'Top content' }
        },
        {
          path: '/investment',
          component: Investment,
          name: 'investment',
          meta: { title: 'Investment' }
        },
        {
          path: '/charity',
          component: Charity,
          name: 'charity',
          meta: { title: 'Charity' }
        },
        {
          path: '/add/feed',
          component: AddFeedPost,
          name: 'addFeedPost',
          meta: { title: 'Add Feed Post' }
        },
        {
          path: '/register',
          component: Register,
          name: 'register',
          meta: { title: 'Register' }
        },
        {
          path: '/login',
          component: Login,
          name: 'login',
          meta: { title: 'Login' }
        }
      // ]
    // }
  ]
})

router.replace('/feed')

export default router
