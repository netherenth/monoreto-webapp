import Vue from 'vue'
import Vuex from 'vuex'

import httpClient from './utils/httpClient'

Vue.use(Vuex)

const store = new Vuex.Store({
  state: {
    mobile: {
      isProfileVisible: false,
      isExpandedFooterVisible: false
    },
    likesCounter: 199,
    investedCounter: 109,
    donatedCounter: 399,
    likeFilter: {
      feed: false,
      topContent: false,
      investment: false,
      charity: false
    },
    isPopupOpened: false,
    homeGender: 0, // 0 - female, 1 - male
    createAccFormSubmitted: false,
    smbHasLiked: false,
    balance: 197,
    balanceUsd: 9.85,
    jwtToken: null,
    addPostType: 'feed'
  },
  mutations: {
    setLikesCounter (state, number) {
      state.likesCounter = number
    },
    setInvestedCounter (state, number) {
      state.investedCounter = number
    },
    setDonatedCounter (state, number) {
      state.donatedCounter = number
    },
    mobileShowProfile (state) {
      state.mobile.isProfileVisible = true
    },
    mobileHideProfile (state) {
      state.mobile.isProfileVisible = false
    },
    mobileShowExpandedFooter (state) {
      state.mobile.isExpandedFooterVisible = true
    },
    mobileHideExpandedFooter (state) {
      state.mobile.isExpandedFooterVisible = false
    },
    toggleLikeFilter (state, screen) {
      state.likeFilter[screen] = !state.likeFilter[screen]
    },
    disableLikeFilter (state, screen) {
      state.likeFilter[screen] = false
    },
    // Deprecated
    togglePopup (state) {
      state.isPopupOpened = !state.isPopupOpened
    },
    hidePopup (state) {
      state.isPopupOpened = false
    },
    showPopup (state) {
      state.isPopupOpened = true
    },
    setHomeGender (state, gender) {
      state.homeGender = gender
    },
    setCreateAccFormSubmitted (state, email = '') {
      let params = new URLSearchParams()
      params.append('email', email)
      state.createAccFormSubmitted = true // optimistic update
      httpClient.post(`/register`, params, {
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded'
        }
      })
      .then(res => {
        if (res.status === 200) {
        //   state.createAccFormSubmitted = true
        }
      })
      .catch(err => console.error(err))
    },
    smbHasLikedTrigger (state) {
      state.smbHasLiked = !state.smbHasLiked
      if (state.smbHasLiked) {
        state.balance += 1
        state.balanceUsd += 0.05
      }
    },
    setJwtToken (state, token) {
      state.jwtToken = token
    },
    setAddPostType (state, type) {
      state.addPostType = type
    }
  }
})

export default store

setTimeout(function triggerHasLiked () {
  const minDelay = 20000
  let delay = Math.random() * minDelay
  if (delay < minDelay) {
    delay = minDelay
  }
  setTimeout(() => {
    store.commit('smbHasLikedTrigger')
    setTimeout(() => {
      store.commit('smbHasLikedTrigger')
      triggerHasLiked()
    }, 7000)
  }, delay)
}, 1000)
