FRONTEND
========
`yarn` or `npm i`
tweak `src/utils/httpClient.js` if needed

Development:
`yarn serve` or `npm run serve` for local development

Production:
set `baseUrl: '/'` in `vue.config.js`,
`yarn build` or `npm run build` for production build
`cd dist` and run your server (e.g.: `php -S localhost:8000`)

BACKEND
=======
(This setup is optional)
`application/config/database.php` => database settings
`application/controllers/api/Register.php` => mailer settings
`composer install`
import `monoreto.sql`
